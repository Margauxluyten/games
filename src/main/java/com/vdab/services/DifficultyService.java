package com.vdab.services;

import com.vdab.domain.Difficulty;
import com.vdab.repositories.DifficultyRepository;

import java.util.List;

public class DifficultyService {

    private DifficultyRepository difficultyRepository = new DifficultyRepository();

    public List<Difficulty> findAll() {
        return difficultyRepository.findAll();
    }
}
