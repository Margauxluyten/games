package com.vdab.services;

import com.vdab.domain.Category;
import com.vdab.repositories.CategoryRepository;

import java.util.List;

public class CategoryService {

    CategoryRepository categoryRepository = new CategoryRepository();

    public Category findCategoryOne() {
        return categoryRepository.findCategoryOne();
    }

    public List<Category> findAll() {
        return categoryRepository.findAll();
    }
}
