package com.vdab.controllers;

import com.vdab.domain.Game;
import com.vdab.services.CategoryService;
import com.vdab.services.DifficultyService;
import com.vdab.services.GameService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class GamesController {

    private GameService gameService = new GameService();
    private DifficultyService difficultyService = new DifficultyService();
    private CategoryService categoryService = new CategoryService();

    @GetMapping(value = "/")
    public String showGamePage(Model model) {
        model.addAttribute("allGames", gameService.showAllGames());
        return "index";
    }

    @GetMapping(value = "/newgame")
    public String showAddGamePage(Model model) {
        //TODO: Gebruik een difficulty service en difficulty repo op de difficulties op te halen van database
//        List<Difficulty> difficultyList = new ArrayList<>();
//        difficultyList.add(Difficulty.builder().id(1).difficultyName("very easy").build());
//
//        List<Category> categoryList = new ArrayList<>();
//        categoryList.add(Category.builder().id(1).categoryName("combination").build());

        model.addAttribute("difficulties", difficultyService.findAll());
        model.addAttribute("cats", categoryService.findAll());
        model.addAttribute("newGame", new Game());
        return "addgames";
    }

    @PostMapping(value = "/savegame")
    public String saveAGame(@ModelAttribute Game newGame) {
        gameService.save(newGame);
        return "redirect:/";
    }

    @GetMapping(value = "/delete/{gameId}")
    public String deleteAGame(@PathVariable("gameId") int id){
        gameService.deleteById(id);
        return "redirect:/";
    }
}

