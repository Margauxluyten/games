package com.vdab.repositories;

import com.vdab.domain.Category;
import com.vdab.domain.Difficulty;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class CategoryRepository {

    public Category findCategoryOne() {
        try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/games","root","P@ssw0rd")) {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from category where id = 1 ");
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();
            resultSet.next();

            // dit is wat we gaan oproepen via builder
            return Category.builder().id(resultSet.getInt("id")).categoryName(resultSet.getString("category_name")).build();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public List<Category> findAll() {
        List<Category> categoryList = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/games", "root", "P@ssw0rd")) {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from category");
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();

            while (resultSet.next()) {
                categoryList.add(
                        Category.builder().id(resultSet.getInt("id")).categoryName(resultSet.getString("category_name")).build()
                );
            }
            return categoryList;
        } catch (Exception e) {
            e.printStackTrace();
            return categoryList;
        }

    }
}
