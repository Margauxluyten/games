package com.vdab.repositories;

import com.vdab.domain.Category;
import com.vdab.domain.Difficulty;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class DifficultyRepository {
    public List<Difficulty> findAll() {
        List<Difficulty> difficultyList = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/games", "root", "P@ssw0rd")) {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from difficulty");
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();

            while (resultSet.next()) {
                difficultyList.add(
                        Difficulty.builder()
                                .id(resultSet.getInt("id"))
                                .difficultyName(resultSet.getString("difficulty_name"))
                                .build()
                );
            }
            return difficultyList;
        } catch (Exception e) {
            e.printStackTrace();
            return difficultyList;
        }

    }
}
